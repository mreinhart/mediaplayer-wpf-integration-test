﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AxWMPLib;

namespace Mediaplayer_Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> mediaFileList;
        string mediaFolder = String.Empty;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnBrowse_OnClick(object sender, RoutedEventArgs e)
        {
            mediaFileList = new List<string>();
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog(new WindowWrapper(this)) != System.Windows.Forms.DialogResult.Cancel)
            {
                mediaFolder = fbd.SelectedPath;
                var dirInfo = new DirectoryInfo(mediaFolder);
                foreach (var fileInfo in dirInfo.GetFiles())
                {
                    if (fileInfo.Extension == ".wav" || fileInfo.Extension == ".mp3")
                    {
                        mediaFileList.Add(fileInfo.Name);
                    }
                }

                if (mediaFileList != null)
                {
                    lbMediaFiles.ItemsSource = null;
                    lbMediaFiles.ItemsSource = mediaFileList;
                }
            }
        }

        private void LbMediaFiles_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbMediaFiles.SelectedIndex != -1)
            {
                AxWindowsMediaPlayer axWmp = winFormsHost.Child as AxWindowsMediaPlayer;
                activeXMediaPlayer.uiMode = "full";
                activeXMediaPlayer.enableContextMenu = false;
                if (axWmp != null)
                {
                    activeXMediaPlayer.URL = mediaFolder + "\\" + lbMediaFiles.SelectedItem;
                }
            }

            e.Handled = true;
        }
    }

    public class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        public WindowWrapper(IntPtr handle)
        {
            Handle = handle;
        }

        public WindowWrapper(Window window)
        {
            Handle = new WindowInteropHelper(window).Handle;
        }

        public IntPtr Handle { get; }
    }
}
